import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TableUnitTest {

	@Test
	void test() {
		fail("Not yet implemented");
	}
	public void testcheckRow1() {
        Player O = new Player('O');
        Player X = new Player('X');
        Table table = new Table(O, X);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.setRowCol(2, 3);
        assertEquals(true, table.checkWin());
    }
	
	public void testcheckRow2() {
        Player O = new Player('O');
        Player X = new Player('X');
        Table table = new Table(O, X);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.setRowCol(1, 3);
        assertEquals(true, table.checkWin());
    }
	public void testcheckRow3() {
        Player O = new Player('O');
        Player X = new Player('X');
        Table table = new Table(O, X);
        table.setRowCol(3, 1);
        table.setRowCol(3, 2);
        table.setRowCol(3, 3);
        assertEquals(true, table.checkWin());
    }
	
	public void testSwitchTurn() {
        Player O = new Player('O');
        Player X = new Player('X');
        Table table = new Table(O, X);
        table.switchTurn();
        assertEquals('X', table.getCurrentPlayer().getName());
    }

	public void testcheckRight() {
        Player O = new Player('O');
        Player X = new Player('X');
        Table table = new Table(O, X);
        table.setRowCol(1, 3);
        table.setRowCol(2, 2);
        table.setRowCol(3, 1);
        assertEquals(true, table.checkWin());

    }

	public void testcheckLeft() {
        Player O = new Player('O');
        Player X = new Player('X');
        Table table = new Table(O, X);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.setRowCol(3, 3);
        assertEquals(true, table.checkWin());

    }
	
	public void testcheckCol1() {
        Player O = new Player('O');
        Player X = new Player('X');
        Table table = new Table(O, X);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.setRowCol(3, 1);
        assertEquals(true, table.checkWin());

    }
	
	public void testcheckCol2() {
        Player O = new Player('O');
        Player X = new Player('X');
        Table table = new Table(O, X);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.setRowCol(3, 2);
        assertEquals(true, table.checkWin());

    }
	

	public void testcheckCol3() {
        Player O = new Player('O');
        Player X = new Player('X');
        Table table = new Table(O, X);
        table.setRowCol(1, 3);
        table.setRowCol(2, 3);
        table.setRowCol(3, 3);
        assertEquals(true, table.checkWin());

    }

	public void checkDraw() {
        Player O = new Player('O');
        Player X = new Player('X');
        Table table = new Table(O, X);
        table.setRowCol(1, 1);
        table.switchTurn();
        table.setRowCol(2, 1);
        table.switchTurn();
        table.setRowCol(2, 2);
        table.switchTurn();
        table.setRowCol(3, 2);
        table.switchTurn();
        table.setRowCol(3, 1);
        table.switchTurn();
        table.setRowCol(1, 3);
        table.switchTurn();
        table.setRowCol(1, 2);
        table.switchTurn();
        table.setRowCol(3, 3);
        table.switchTurn();
        table.setRowCol(2, 3);
        assertEquals(true, table.checkDraw());

    }
	
}
